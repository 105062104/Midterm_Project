# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [shopping website]
* Key functions (add/delete)
    1. product page
    2. shopping pipeline
    3. user dashboard
    4. sign in/sign up
    5. database read/write
    6. works fine with smaller screen
* Other functions (add/delete)
    1. sign up/in with google
    2. CSS animation

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
    1. 首頁
        首頁左側欄可選進入商品網頁或連結到外部網站（認養地圖），點擊下方六張圖片的商品名稱即可進入該商品的詳細解說。
        上方欄位有log in, user profile, cart, log out。使用者尚未登入時不會顯示log out 只會顯示log in、user profile 和 cart，使用者已等入時不會顯示log in 只會顯示user profile、cart 和 log out。
    2. log in
        點擊首頁的log in 可進入，若已註冊過，可直接輸入信箱與密碼，輸入完按log in；或者按log in with google的圖案，會通過pop out 視窗登入，若成功登入會直接跳轉回首頁。若沒有成功登入會顯示alert（原因）。若尚未註冊過，底下有sign up ，點擊即可進入sign up 頁面。
    3. sign up
        要求要輸入name、email 和 password，成功建立user 後跳轉回首頁，若註冊失敗會顯示alert（原因）。也可以點擊google圖案，通過pop out 視窗登入。
    4. user profile 
        若使用者已等入，會顯示目前使用者的名字與信箱，若曾購買過商品則會一併顯示當時填寫的聯絡方式與地址，每次顯示的都是最新一次填寫的資料。左側欄有log out 鍵，按下可登出。若使用者尚未登入，會顯示alert（you hasn't logged in yet)，並跳轉至log in 頁面。點擊 Woof 字樣可回首頁。
    5. 個別商品頁面
        左側欄為商品一覽表，中間則是商品圖案、名稱、價錢與詳細說明。在商品名稱右方有add to cart的按鈕，按下可將該商品加入購物車。
    6. shopping cart
        顯示目前已放入購物車的商品與該商品價錢，若欲刪除商品，在標了delete 的地方輸入欲刪除的商品並點擊confirm，即可將該商品一項刪除。點選continue shopping會跳轉至商品頁面，點選clear cart 會將購物車清空，下方欄位會計算商品總額以及運費。滿799免運，低於799加收60元運費。subtotal 顯示商品總額，shipping 顯示運費，total 是全部應付金額。右下角的top點選後會回到該頁面最上方。按proceed to checkout 會跳轉至check out 頁面。
    7. checkout
        首先進入的是輸入信用卡資料的頁面，輸入信用卡號碼、名字、到期日期後，按confirm後輸入地址與聯絡方式，再按confirm 即完成付帳。該筆資訊會傳回database儲存，同時購物車將自動清空。

    所使用的API:firebase.js/firebase.auth/firebase.database/firebase.User
    所使用的template:
        shopping page/shopping item: https://startbootstrap.com/template-overviews/shop-item/
        shopping cart: https://colorlib.com/wp/template/karl/
        checkout: https://dcrazed.net/free-html5-css3-checkout-forms/
## Security Report (Optional)
    firebase database 的 read 和 write 只有已授權的登入者才能使用。
    自己的資料會存在自己的userID下，傳data進database時，只會存在自己的userID下，讀data時，也只會讀取自己的userID下的東西。每個userID都是unique的，因此不會寫入或讀取到別人的資料，藉此實現security。