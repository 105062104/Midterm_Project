var txtName = document.getElementById('txtName');
var txtEmail = document.getElementById('txtEmail');
var txtPassword = document.getElementById('txtPassword');
var btnLogin = document.getElementById('btnLogin');
var btnLogout = document.getElementById('btnLogout');

btnLogin.addEventListener('click',e =>{
    var name = txtName.value;
    var email = txtEmail.value;
    var password = txtPassword.value;

    firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
      user = firebase.auth().currentUser;
      alert("sign up success");
      //user.sendEmailVerification();
      txtEmail.value = "";
      txtPassword.value = "";
    })
    .then(function () {
      user.updateProfile({
        displayName: name,
        uid: user.uid
      })
      .then(function(){
        console.log(user.displayName);
        window.location.href = "index.html";
        })
    })
    .catch(function(error) {
      console.log(error.message);
      //alert(email);
    });
    console.log('Validation link was sent to ' + email + '.');
    
    //
});

// Login with Google
var btnLoginGooglePop = document.getElementById('btnLoginGooglePop');
var btnLoginGoogleRedi = document.getElementById('btnLoginGoogleRedi');

var provider = new firebase.auth.GoogleAuthProvider();

btnLoginGooglePop.addEventListener('click', e => {
    console.log('signInWithPopup');
    firebase.auth().signInWithPopup(provider).then(function (result) {
        var token = result.credential.accessToken;
        var user = result.user;
        window.location.href = "index.html";
    }).catch(function (error) {
        alert(e.message);
        console.log('error: ' + error.message);
        create_alert("error",error.message);
    });
});