// Login with Email/Password
var txtEmail = document.getElementById('txtEmail');
var txtPassword = document.getElementById('txtPassword');
var btnLogin = document.getElementById('btnLogin');
var btnSignUp = document.getElementById('btnSignUp');
var btnLogout = document.getElementById('btnLogout');

btnLogin.addEventListener('click', e => {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(e => {
        window.location.href = "index.html";
    }).catch(e => {
        console.log(e.message);
        alert(e.message);
        //create_alert("error",e.message);
        txtEmail.value = " ";
        txtPassword.value = " ";
    });
});

/*btnSignUp.addEventListener('click', e => {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().createUserWithEmailAndPassword(email, password).then(e =>{       
        window.location.href = "index.html";
        //create_alert("success",success.message);
        txtEmail.value = " ";
        txtPassword.value = " ";
    }).catch(e => {
        console.log(e.message);
        alert(e.message);
        //create_alert("error",error.message);
        txtEmail.value = " ";
        txtPassword.value = " ";
    });
});*/

// Login with Google
var btnLoginGooglePop = document.getElementById('btnLoginGooglePop');
var btnLoginGoogleRedi = document.getElementById('btnLoginGoogleRedi');

var provider = new firebase.auth.GoogleAuthProvider();

btnLoginGooglePop.addEventListener('click', e => {
    console.log('signInWithPopup');
    firebase.auth().signInWithPopup(provider).then(function (result) {
        var token = result.credential.accessToken;
        var user = result.user;
        window.location.href = "index.html";
    }).catch(function (error) {
        alert(e.message);
        console.log('error: ' + error.message);
        create_alert("error",error.message);
    });
});

/*btnLoginGoogleRedi.addEventListener('click', e => {
    console.log('signInWithPopup');
    firebase.auth().signInWithRedirect(provider);
});

firebase.auth().getRedirectResult().then(function (result) {
    if (result.credential)
        var token = result.credential.accessToken;
    var user = result.user;
}).catch(function (error) {
    console.log('error: ' + error.message);
});*/

// Login with Facebook
/*var btnLoginFBPop = document.getElementById('btnLoginFBPop');
var btnLoginFBRedi = document.getElementById('btnLoginFBRedi');

var facebook_provider = new firebase.auth.FacebookAuthProvider();

btnLoginFBPop.addEventListener('click', e => {
    firebase.auth().signInWithPopup(facebook_provider).then(function (result) {
        console.log('signInWithPopup');
        var token = result.credential.accessToken;
        var user = result.user;
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
    });
});

btnLoginFBRedi.addEventListener('click', e => {
    firebase.auth().signInWithRedirect(facebook_provider);
});*/

// Manage Users
/*var usrDiv = document.getElementById('usrDiv');
var usrName = document.getElementById('usrName');
var useEmail = document.getElementById('useEmail');

firebase.auth().onAuthStateChanged(user => {
    if (user) {
        console.log(user);
        usrDiv.style.visibility = 'visible';
        usrName.innerHTML = 'User Name: ' + user.displayName;
        useEmail.innerHTML = 'User Email: ' + user.email;
        
        //login_form.style.visibility = 'hidden';
        //google_login_form.style.visibility = 'hidden';
        //signup_form.style.visibility = 'hidden';
    } else {
        console.log('not logged in');
        usrDiv.style.visibility = 'hidden';

        //login_form.style.visibility = 'visible';
        //google_login_form.style.visibility = 'visible';
        //signup_form.style.visibility = 'visible';
        //btnLogout.classList.add('hide');
    }
});*/

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}