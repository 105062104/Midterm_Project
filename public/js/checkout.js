
var confirm = document.getElementById('confirm');
var first = document.getElementById('first');
var second = document.getElementById('second');
var third = document.getElementById('third');
var fourth = document.getElementById('fourth');
var cardname = document.getElementById('cardname');
var expiry = document.getElementById('expiry');
var year = document.getElementById('year');
var csv = document.getElementById('csv');

var user = firebase.auth().currentUser;


var database = firebase.database();
var update = {};

firebase.auth().onAuthStateChanged(function(user) {
    confirm.addEventListener('click', function () {
    
        var cfirst = first.value;
        var csecond = second.value;
        var cthird = third.value;
        var cfourth = fourth.value;
        var cname = cardname.value;
        var cyear = year.value;
        var ccsv = csv.value;
        console.log(cname);
        var uid = user.uid;
        var paymentRef = database.ref().child('Payment').child(uid);
    
        var updates = {
            first: cfirst,
            second: csecond, 
            third: cthird,
            fourth: cfourth,
            Name: cname,
            Year: cyear,
            Csv: ccsv
        };
        paymentRef.push(updates);
        
        window.location.href = "address.html";
        
    });

});