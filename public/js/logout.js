
var btnLogout = document.getElementById('btnLogout');
var btnDonnotlogout = document.getElementById('btnDonnotlogout');

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
        console.log('logged in');
    
    } else {
      // No user is signed in.
        console.log('not logged in');
        //alert("You are logging out!");
        window.location.href = "index.html";
    }
  });

btnLogout.addEventListener('click', e => { 
    firebase.auth().signOut();
});


btnDonnotlogout.addEventListener('click', e => { 
  window.location.href = "index.html";
});


