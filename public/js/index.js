// Manage Users
var user = firebase.auth().currentUser;
var nav_login = document.getElementById('nav_login');
var nav_logout = document.getElementById('nav_logout');
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
        console.log('logged in');
        nav_login.style.visibility = 'hidden';
        nav_logout.style.visibility = 'visible';
    } else {
        // No user is signed in.
        console.log('not logged in');
        nav_login.style.visibility = 'visible';
        nav_logout.style.visibility = 'hidden';
    }
  });
